package cz.uhk.pro1.snake.io;

import cz.uhk.pro1.snake.model.GameBoard;
import cz.uhk.pro1.snake.model.Wall;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileGameBoardReader implements GameBoardReader {

    private final String filename;

    public FileGameBoardReader(String filename){
        this.filename = filename;
    }

    @Override
    public GameBoard read() {
        try (BufferedReader r = new BufferedReader(new FileReader(filename))) {
            String[] s = r.readLine().split(";");
            int gameBoardRows = Integer.parseInt(s[0]);
            int gameBoardColumns = Integer.parseInt(s[1]);

            s = r.readLine().split(";");
            int tileWidth = Integer.parseInt(s[0]);
            int tileHeight = Integer.parseInt(s[1]);

            s = r.readLine().split(";");
            Color snakeColor = Color.decode(s[0]);

            s = r.readLine().split(";");
            Color bonusColor = Color.decode(s[0]);

            s = r.readLine().split(";");
            Color wallColor = Color.decode(s[0]);

            s = r.readLine().split(";");
            Color backgroundColor = Color.decode(s[0]);

            GameBoard gb = new GameBoard(gameBoardColumns, gameBoardRows, tileWidth, tileHeight);
            //TODO barvy odeslat a pouzit

            // cteme jednotlive radky popisu herni plochy
            for(int i = 0; i < gameBoardRows; i++){
                s = r.readLine().split(";");
                for(int j = 0; j < s.length; j++){
                    if (s[j].equals("X")){
                        // na dlazdici se souradnici i, j ma byt zed
                        gb.addWall(new Wall(i, j, wallColor));
                    }
                }
            }
            return gb;


        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error reading gameboard file" + filename, e);
        } catch (IOException e) {
            throw new RuntimeException("Error reading gameboard file" + filename, e);
        }
    }


}
