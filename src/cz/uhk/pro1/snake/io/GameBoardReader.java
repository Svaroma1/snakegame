package cz.uhk.pro1.snake.io;

import cz.uhk.pro1.snake.model.GameBoard;

public interface GameBoardReader {
    /**
     * Reads a configuration and creates a GameBoard on the configuration
     * @return created GameBoard
     */

    GameBoard read();
}
