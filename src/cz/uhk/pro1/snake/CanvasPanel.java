package cz.uhk.pro1.snake;

import cz.uhk.pro1.snake.model.GameBoard;

import javax.swing.*;
import java.awt.*;


public class CanvasPanel extends JPanel {

    private final GameBoard gb;

    public CanvasPanel(GameBoard gb){
        this.gb = gb;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        /* g.setColor(Color.BLACK);
        g.fillRect(0,0,80,80);*/
        gb.paint(g);
    }
}
