package cz.uhk.pro1.snake;

import cz.uhk.pro1.snake.io.FileGameBoardReader;
import cz.uhk.pro1.snake.io.GameBoardReader;
import cz.uhk.pro1.snake.model.GameBoard;
import cz.uhk.pro1.snake.model.SnakeDirection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class MainWindow extends JFrame {

    public final Timer timer = new Timer(200, e -> tick());
    public final GameBoard gb;
    public final CanvasPanel p;

    public MainWindow() {
        GameBoardReader r = new FileGameBoardReader("game.csv");
        gb = r.read();
        p = new CanvasPanel(gb);

        setTitle("Snake");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(p);
        p.setPreferredSize(new Dimension(500,500));
        pack(); //nastaví rozměr okna JFrame podle JPanelu
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()){
                    case KeyEvent.VK_LEFT -> {
                        gb.setSnakeDirection(SnakeDirection.WEST);
                        break;
                    }
                    case KeyEvent.VK_RIGHT -> {
                        gb.setSnakeDirection(SnakeDirection.EAST);
                        break;
                    }
                    case KeyEvent.VK_UP -> {
                        gb.setSnakeDirection(SnakeDirection.NORTH);
                        break;
                    }
                    case KeyEvent.VK_DOWN -> {
                        gb.setSnakeDirection(SnakeDirection.SOUTH);
                        break;
                    }
                    case KeyEvent.VK_P -> {
                        if(timer.isRunning()){timer.stop();
                            getGraphics().fillRect(220,215,26,100);
                            getGraphics().fillRect(265,215,26,100);
                            System.out.println("Paused");}
                        else{timer.start();}
                    }
                }
            }
        });
        timer.start();
    }

    public void tick() {
        boolean shouldContinue = gb.tick();
        repaint();

        if(!shouldContinue){
            System.out.println("GameOver");
            timer.stop();
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainWindow w = new MainWindow();
            w.setVisible(true);
        });

    }


    }




