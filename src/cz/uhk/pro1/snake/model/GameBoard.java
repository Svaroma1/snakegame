package cz.uhk.pro1.snake.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameBoard {
    private final Random r = new Random();
    private final int columns;
    private final int rows;
    private final int tileWidth;
    private final int tileHeight;


    public Snake s = new Snake(2, 2, SnakeDirection.EAST);
    List<Bonus> bonuses = new ArrayList<Bonus>();
    List<Wall> walls = new ArrayList<>();

    public GameBoard(int gameBoardColumns, int gameBoardRows, int tileWidth, int tileHeight) {
        this.columns = gameBoardColumns;
        this.rows = gameBoardRows;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;

        Bonus b1 = new Bonus(2, 2, Color.RED); //TODO vzit hodnotu z readeru
        Bonus b2 = new Bonus(12, 8, Color.RED);
        Bonus b3 = new Bonus(5, 10, Color.RED);
        bonuses.add(b1);
        bonuses.add(b2);
        bonuses.add(b3);
    }


    public void paint(Graphics g) {
        s.paint(g);
        for (Bonus b : bonuses) {
            b.paint(g);
        }

        for (Wall w : walls) {
            w.paint(g);
        }
    }

    /**
     * Moves a game to a new state (e.g. moves a snake)
     *
     * @return false if the game is over
     */
    public boolean tick() {
        SnakePiece newHead = s.getNewHead();
        boolean bonusConsumed = false;

        List<Bonus> bonusesCopy = new ArrayList<>(bonuses);
        for (Bonus b : bonusesCopy) {
            if (newHead.collidesWith(b)) {
                // sezrat bonus
                bonuses.remove(b); // odstranime bonus ze seznamu
                bonusConsumed = true;


                Bonus x = new Bonus(r.nextInt(columns), r.nextInt(rows), Color.RED);
                bonuses.add(x);

                for (Wall w : walls) {
                    for (SnakePiece sp : s.getPieces()) {
                        while (x.collidesWith(sp) || x.collidesWith(w)) {
                            bonuses.remove(x);
                            x = new Bonus(r.nextInt(columns), r.nextInt(rows), Color.RED);
                            bonuses.add(x);
                        }
                    }
                }
                System.out.println("New bonus at: " + b.i + " " + b.j);
            }
        }





        for (SnakePiece sp : s.getPieces()){
            if (newHead.collidesWith(sp)){
                return false;
            }
        }

        for(Wall w : walls){
            if((newHead.collidesWith(w))){
                return false;
            }
        }

        s.getPieces().addFirst(newHead);
        if (!bonusConsumed) {
            s.getPieces().removeLast();
        }



        System.out.println("Tick");
    return true;
    }

    public void setSnakeDirection(SnakeDirection direction){
        s.setDirection(direction);
    }


    public void addWall(Wall wall) {
        walls.add(wall);
    }

}
