package cz.uhk.pro1.snake.model;
import cz.uhk.pro1.snake.MainWindow;

import cz.uhk.pro1.snake.io.FileGameBoardReader;

import java.awt.*;

public abstract class Tile {
    protected final int i;
    protected final int j;
    protected final Color color;



    public Tile(int i, int j, Color color) {
        this.i = i;
        this.j = j;
        this.color = color;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillRect(j*25,i*25, 25,25);
    }

    public boolean collidesWith(Tile t) {
        return(i == t.i && j == t.j);
    }
}

