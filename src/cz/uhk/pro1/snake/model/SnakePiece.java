package cz.uhk.pro1.snake.model;

import java.awt.*;

public class SnakePiece extends Tile{

    public SnakePiece(int i, int j, Color color) {
        super(i, j, color);
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

}
