package cz.uhk.pro1.snake.model;

import java.awt.*;
import java.util.Deque;
import java.util.LinkedList;

public class Snake {
    private final Deque<SnakePiece> pieces = new LinkedList<>();
    private SnakeDirection direction;
    private Color color = Color.CYAN;


    public Snake(int initiali, int initialj, SnakeDirection direction){
        SnakePiece p = new SnakePiece(initiali, initialj, color);
        this.direction = direction;
        pieces.add(p);
    }
    
    public void paint(Graphics g) {
        for (SnakePiece p : pieces
             ) { p.paint(g);

        }
    }

    public Deque<SnakePiece> getPieces() {
        return pieces;
    }


    public void setDirection(SnakeDirection direction){
        this.direction = direction;
    }

    public SnakePiece getNewHead() {
        SnakePiece head = pieces.getFirst();
        SnakePiece newHead;

        switch(direction){
            case NORTH -> {
                newHead = new SnakePiece(head.getI() - 1, head.getJ(), color);
                break;
            }
            case SOUTH -> {
                newHead = new SnakePiece(head.getI() + 1, head.getJ(), color);
                break;
            }
            case EAST -> {
                newHead = new SnakePiece(head.getI(), head.getJ() + 1, color);
                break;
            }
            case WEST -> {
                newHead = new SnakePiece(head.getI(), head.getJ() - 1, color);
                break;
            }
            default -> throw new IllegalStateException("Undefined direction value");
        }

        //pieces.addFirst(newHead); // pridat "novou hlavu" na zacatek
        //pieces.removeLast();       // odstranit "starou hlavu" z konce
        return newHead;
    }


    public SnakePiece getHead() {
        return pieces.getFirst();
    }
}
