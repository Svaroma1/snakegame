package cz.uhk.pro1.snake.model;

import java.awt.*;

public class Wall extends Tile{
    public Wall(int i, int j, Color color) {
        super(i, j, color);
    }
}

